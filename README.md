syntax_loadbalancer
=========

Provisions:		
- AWS Loadbalancer service
- Routes HTTP traffic to port 8000


Notes
------------
https routing is not possible without setting up CA/certs or providing domain.
using http for now

Requirements
------------
N/A

Role Variables
--------------
N/A

Dependencies
------------
AWS CLI

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: awscli
      roles:
     - { role: syntax_loadbalancer, lb.name=${LB.NAME}, ld.listeners='protocol:https...' }

License
-------

GNU

See also
------------------
N/A
